import { NextPage } from 'next';
import { Fragment } from 'react';

import styles from './index.module.css';

const ApiOrigin = 'http://localhost:8686';

interface Props {
    userAgent?: string;
    loads: any;
    jumpDate: string;
}

enum JumpType {
    Tandem = 'T',
    TandemHandcamera = 'THK',
    Photo = 'V',
    Pax = 'P',
}

enum LoadStatus {
    Planed = 1, //Planerad
    booked = 2, //Bokad
    lifted = 3, //Lyft
    droped = 4, //Fällt
    landed = 5, //Landat
}

type Jump = {
    jumpNo: number;
    captain: true;
    jumpType: JumpType;
    jumpTypeName: 'Tandem' | 'Tandem Handkamera' | 'Fotograf' | 'TandemPax';
    weight: number;
    name: string;
    memberNo: number;
};

type Group = {
    jumps: [Jump];
};

type Load = {
    boogie: string;
    regDate: string;
    planeReg: string;
    loadNo: number;
    loadStatus: LoadStatus;
    loadStatusName: string;
    nostat: boolean;
    liftTime: string;
    comment: string;
    groups: [Group];
};

type Loads = [Load];

function getLoadStatusText(loadStatus: LoadStatus): string {
    switch (loadStatus) {
        case LoadStatus.Planed:
            return 'Planerad';
        case LoadStatus.booked:
            return 'Bokad';
        case LoadStatus.lifted:
            return 'Lyft';
        case LoadStatus.droped:
            return 'Hoppat';
        case LoadStatus.landed:
            return 'Landat';
    }
}

function getJumpType(jumpType: string) {
    switch (jumpType) {
        case JumpType.Tandem:
            return 'Tandem pilot';
        case JumpType.TandemHandcamera:
            return 'Tandem HK';
        case JumpType.Pax:
            return 'Passagerare';
        case JumpType.Pax:
            return 'Passagerare';
        default:
            return jumpType;
    }
}

function TandemGroups({ groups }: any) {
    const tandemGroups = groups.map((group, index) => {
        const tandemGroup = group.map((jumper: Jump, index: number) => {
            console.log(jumper);

            return (
                <Fragment key={`tg-${index}`}>
                    {`${getJumpType(jumper.jumpType)}: ${jumper.name} `}
                </Fragment>
            );
        });
        return (
            <div key={`tg-${index}`} className={styles.tandemGroup}>
                {tandemGroup}
            </div>
        );
    });
    return <div>{tandemGroups}</div>;
}

function Loads(props: any) {
    const listItems = props.loads.map((load: Load, index) => {
        const { groups, loadStatus } = load;
        let style = styles.column;
        if (index > 4) return null;
        if (loadStatus === LoadStatus.landed) {
            style = styles.landed;
        }

        return (
            <div key={`tr-${index}`} className={style}>
                Nr. {load.loadNo} Flygplan: {load.planeReg}{' '}
                <TandemGroups groups={groups} />
                <div>{getLoadStatusText(loadStatus)}</div>
            </div>
        );
        //return (<tr><td>{load.loadNo},</td></tr>)
    });
    return <div className={styles.main}>{listItems}</div>;
}

const Page: NextPage<Props> = ({ loads, jumpDate }) => (
    <main>
        <Loads loads={filteredLoads(loads)} />
        <div>{jumpDate}</div>
    </main>
);

function filteredLoads(loads: Loads) {
    return loads
        .map((load) => {
            const { groups } = load;
            const groupsWithTandem = groups
                .map((group) => {
                    const { jumps } = group;
                    const jumpsWithTandem = jumps.filter((jump: Jump) => {
                        const { jumpType } = jump;
                        return (
                            jumpType === 'T' ||
                            jumpType === 'THK' ||
                            jumpType === 'V' ||
                            jumpType === 'P'
                        );
                    });
                    return jumpsWithTandem;
                })
                .filter((group) => group.length > 0);
            return { ...load, groups: groupsWithTandem };
        })
        .filter((load) => load?.groups.length > 0);
}

Page.getInitialProps = async ({ req }) => {
    const userAgent = req ? req.headers['user-agent'] : navigator.userAgent;
    const res = await fetch(`${ApiOrigin}/api/loads`);
    const { loads, jumpDate } = await res.json();
    return { userAgent, loads, jumpDate };
};

/* export async function getServerSideProps({ req }) {
    const userAgent = req ? req.headers['user-agent'] : navigator.userAgent;
    const res = await fetch(`${ApiOrigin}/api/loads`);
    const { loads, jumpDate } = await res.json();
    return { userAgent, loads, jumpDate };
}

function App(props) {
    const initialData = props.data;
    const { data } = useSWR('/api/data', fetcher, { initialData });

    return <div>{data}</div>;
} */

export default Page;
