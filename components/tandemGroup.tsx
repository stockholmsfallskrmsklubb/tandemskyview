import { Fragment } from 'react';

function getJumpType(jumpType: string) {
    switch (jumpType) {
        case 'JumpType.Tandem':
            return 'Tandem pilot';
            break;
        case 'JumpType.TandemHandcamera':
            return 'Tandem HK';
            break;
        case 'JumpType.Pax':
            return 'Passagerare';
            break;
        case 'JumpType.Pax':
            return 'Passagerare';
            break;
        default:
            return jumpType;
    }
}

export function TandemGroups({ groups }: any) {
    const tandemGroups = groups.map((group, index) => {
        const tandemGroup = group.map((jumper, index) => {
            console.log(jumper);

            return (
                <Fragment key={`tg-${index}`}>
                    {`${getJumpType(jumper.jumpType)}: ${jumper.name} `}
                </Fragment>
            );
        });
        return <div key={`tg-${index}`}>{tandemGroup}</div>;
    });
    return <div>{tandemGroups}</div>;
}
